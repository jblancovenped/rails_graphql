# 5.times do |i|
#   Note.create(title: "Note #{i + 1}", body: 'Lorem ipsum saves lives')
# end

require "faker"

953.times do |i|
  Product.create(
    sku: Faker::Number.between(from: 100000000000, to: 999999999999),
    title: Faker::Commerce.product_name,
    price: Faker::Commerce.price,
    tax: ["es_general_21", "es_reduced_10", "es_super-reduced_4", "fr_general_20", "fr_reduced_5.5"].sample,
    stock: Faker::Number.between(from: 0, to: 50),
  )
end
