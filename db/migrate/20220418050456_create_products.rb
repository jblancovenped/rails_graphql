class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :sku
      t.string :title
      t.float :price
      t.string :tax
      t.integer :stock

      t.timestamps
    end
    add_index :products, :sku
  end
end
