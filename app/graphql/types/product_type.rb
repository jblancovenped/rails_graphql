module Types
  class ProductType < Types::BaseObject
    field :id, ID, null: false
    field :sku, String, null: false
    field :title, String, null: false
    field :price, Float, null: false
    field :tax, String, null: false
    field :stock, Integer, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
