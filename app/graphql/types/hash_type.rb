# frozen_string_literal: true

class Types::HashType < Types::BaseScalar
  graphql_name 'Hash'
  description 'Hash or Dictionary object'

  def self.coerce_input(value, _context)
    JSON.parse(value)
  end

  def self.coerce_result(value, _context)
    value
  end
end
