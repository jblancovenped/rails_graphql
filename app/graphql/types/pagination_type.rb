module Types
  class PaginationType < Types::BaseObject
    field :total_results, Integer, null: true
    field :limit_value, Integer, null: true
    field :total_pages, Integer, null: true
    field :current_page, Integer, null: true
    field :next_page, Integer, null: true
    field :prev_page, Integer, null: true
    field :first_page, Boolean, null: true
    field :last_page, Boolean, null: true
    field :out_of_range, Boolean, null: true
  end
end
