module Types
  class MutationType < Types::BaseObject
    field :add_note, mutation: Mutations::AddNote
    field :add_product, mutation: Mutations::AddProduct
    field :update_product, mutation: Mutations::UpdateProduct
  end
end
