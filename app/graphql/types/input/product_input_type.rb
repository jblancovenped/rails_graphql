module Types
  module Input
    class ProductInputType < Types::BaseInputObject
      argument :sku, String, required: true
      argument :title, String, required: true
      argument :price, Float, required: true
      argument :tax, String, required: true
      argument :stock, Integer, required: true
    end
  end
end
