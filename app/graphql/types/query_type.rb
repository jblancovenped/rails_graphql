module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :fetch_notes, resolver: Queries::FetchNotes
    field :fetch_note, resolver: Queries::FetchNote

    field :fetch_products, resolver: Queries::FetchProducts
    field :fetch_product, resolver: Queries::FetchProduct

    field :test_field, String, null: false

    def test_field
      "Hello World!"
    end
  end
end
