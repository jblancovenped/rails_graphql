module Types
  class ProductsType < Types::BaseObject
    field :results, [Types::ProductType], null: false do
      argument :tax_filter, [String], required: false
      argument :title_filter, String, required: false
      argument :order_by, String, required: false
      argument :order, String, required: false
      argument :page, Integer, required: true
      argument :per_page, Integer, required: true
    end
    field :pagination, Types::PaginationType, null: false do
      argument :tax_filter, [String], required: false
      argument :title_filter, String, required: false
      argument :order_by, String, required: false
      argument :order, String, required: false
      argument :page, Integer, required: true
      argument :per_page, Integer, required: true
    end

    def results(page:,
                per_page:,
                tax_filter: nil,
                title_filter: nil,
                order_by: nil,
                order: nil)
      products = Product.all
      products = filtered(products, tax_filter, title_filter)
      products = ordered(products, order_by, order)
      products = paginated(products, page, per_page)
      products
    end

    def pagination(page:,
                   per_page:,
                   tax_filter: nil,
                   title_filter: nil,
                   order_by: nil,
                   order: nil)
      products = Product.all
      products = filtered(products, tax_filter, title_filter)
      total_results = products.count
      products = ordered(products, order_by, order)
      products = paginated(products, page, per_page)
      {
        total_results: total_results,
        limit_value: products.limit_value,
        total_pages: products.total_pages,
        current_page: products.current_page,
        next_page: products.next_page,
        prev_page: products.prev_page,
        first_page: products.first_page?,
        last_page: products.last_page?,
        out_of_range: products.out_of_range?,
      }
    end

    private

    def filtered(products, tax_filter, title_filter)
      products = products.where(tax: tax_filter) if tax_filter
      products = products.where("title ILIKE ?", "%#{title_filter}%") if title_filter
      products
    end

    def ordered(products, order_by, order)
      products = products.order("#{order_by} #{order || "ASC"}") if order_by
      products
    end

    def paginated(products, page, per_page)
      products.page(page).per(per_page)
    end
  end
end
