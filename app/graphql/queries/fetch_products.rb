module Queries
  class FetchProducts < Queries::BaseQuery
    type Types::ProductsType, null: false
  end
end
