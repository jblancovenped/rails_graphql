class Product < ApplicationRecord
  validates :sku, uniqueness: true
  validate :tax_include

  private

  def tax_include
    return if ["es_general_21", "es_reduced_10", "es_super-reduced_4", "fr_general_20", "fr_reduced_5.5"].include?(self.tax)

    errors.add(:tax, "it must be an existing one")
  end
end
